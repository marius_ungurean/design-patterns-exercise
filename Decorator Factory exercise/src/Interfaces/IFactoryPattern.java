package Interfaces;

public interface IFactoryPattern {

    MyIntegerDecorator createDecorator(IMyInteger number, String type);
}
