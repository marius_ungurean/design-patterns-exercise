package Interfaces;

public abstract class MyIntegerDecorator implements IMyInteger {

    protected IMyInteger integer;

    public MyIntegerDecorator(IMyInteger integer) {
        this.integer = integer;
    }

    public int getValue() {
        return integer.getValue();
    }

    public boolean isNil() {
        return false;
    }
}
