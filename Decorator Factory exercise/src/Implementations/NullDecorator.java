package Implementations;

import Interfaces.IMyInteger;
import Interfaces.MyIntegerDecorator;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class NullDecorator extends MyIntegerDecorator {


    public NullDecorator(IMyInteger integer) {
        super(integer);
    }

    @Override
    public boolean isNil() {
        return true;
    }

    @Override
    public int getValue() {
        System.out.println("Not Yet Implemented");
        return super.getValue();
    }
}
