package Implementations;

import Interfaces.IFactoryPattern;
import Interfaces.IMyInteger;
import Interfaces.MyIntegerDecorator;

public class DecoratorFactory implements IFactoryPattern {


    @Override
    public MyIntegerDecorator createDecorator(IMyInteger number, String type) {
        type = type.toLowerCase();
        switch (type) {
            case "addition":
                return new AdditionDecorator(number);
            case "subtraction":
                return new SubtractionDecorator(number);
            case "multiplication":
                return new MultiplicationDecorator(number);
            case "division":
                return new DivisionDecorator(number);
            default:
                return new NullDecorator(number);
        }
    }
}
