package Implementations;

import Interfaces.IMyInteger;
import Interfaces.MyIntegerDecorator;

public class MultiplicationDecorator extends MyIntegerDecorator {

    public MultiplicationDecorator(IMyInteger integer) {
        super(integer);
    }

    @Override
    public int getValue() {
        return super.getValue() * 2;
    }
}
