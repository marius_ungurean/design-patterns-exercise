package Implementations;

import Interfaces.IMyInteger;
import Interfaces.MyIntegerDecorator;

public class SubtractionDecorator extends MyIntegerDecorator {

    public SubtractionDecorator(IMyInteger integer) {
        super(integer);
    }

    @Override
    public int getValue() {
        return super.getValue() - 2;
    }
}
