package Implementations;

import Interfaces.IMyInteger;

public class MyIntegerImpl implements IMyInteger {
    private int value;

    public MyIntegerImpl(int value) {
        this.value = value;
    }

    @Override
    public int getValue() {
        return value;
    }
}
