package Implementations;

import Interfaces.IMyInteger;
import Interfaces.MyIntegerDecorator;

public class AdditionDecorator extends MyIntegerDecorator {

    public AdditionDecorator(IMyInteger integer) {
        super(integer);
    }

    @Override
    public int getValue() {
        return super.getValue() + 2;
    }
}
