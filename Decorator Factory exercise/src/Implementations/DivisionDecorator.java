package Implementations;

import Interfaces.IMyInteger;
import Interfaces.MyIntegerDecorator;

public class DivisionDecorator extends MyIntegerDecorator {

    public DivisionDecorator(IMyInteger integer) {
        super(integer);
    }

    @Override
    public int getValue() {
        return super.getValue() / 2;
    }
}
