import Implementations.DecoratorFactory;
import Implementations.MyIntegerImpl;
import Interfaces.IFactoryPattern;
import Interfaces.IMyInteger;
import Interfaces.MyIntegerDecorator;

import static org.junit.jupiter.api.Assertions.*;

class DecoratorFactoryTest {

    IMyInteger integer;
    IFactoryPattern decoratorFactory;

    @org.junit.jupiter.api.BeforeEach
    void setUp() {
        integer = new MyIntegerImpl(2);
        decoratorFactory = new DecoratorFactory();
    }

    @org.junit.jupiter.api.Test
    void createAdditionDecorator() {
        MyIntegerDecorator decorator = decoratorFactory.createDecorator(integer, "addition");
        assertNotNull(decorator);
        assertEquals(integer.getValue() + 2, decorator.getValue());
        assertEquals(2, integer.getValue());
    }

    @org.junit.jupiter.api.Test
    void createSubtractionDecorator() {
        MyIntegerDecorator decorator = decoratorFactory.createDecorator(integer, "subtraction");
        assertNotNull(decorator);
        assertEquals(integer.getValue() - 2, decorator.getValue());
        assertEquals(2, integer.getValue());
    }

    @org.junit.jupiter.api.Test
    void createMultiplicationDecorator() {
        MyIntegerDecorator decorator = decoratorFactory.createDecorator(integer, "multiplication");
        assertNotNull(decorator);
        assertEquals(integer.getValue() * 2, decorator.getValue());
        assertEquals(2, integer.getValue());
    }

    @org.junit.jupiter.api.Test
    void createDivisionDecorator() {
        MyIntegerDecorator decorator = decoratorFactory.createDecorator(integer, "division");
        assertNotNull(decorator);
        assertEquals(integer.getValue() / 2, decorator.getValue());
        assertEquals(2, integer.getValue());
    }

    @org.junit.jupiter.api.Test
    void createNullDecorator() {
        MyIntegerDecorator decorator = decoratorFactory.createDecorator(integer, "Random");
        assertTrue(decorator.isNil());
        assertEquals(2, decorator.getValue());
        assertEquals(2, integer.getValue());
    }

    @org.junit.jupiter.api.Test
    void wrapMultipleDecorators() {
        MyIntegerDecorator add = decoratorFactory.createDecorator(integer, "addition");
        MyIntegerDecorator multiply = decoratorFactory.createDecorator(add, "multiplication");
        MyIntegerDecorator divide = decoratorFactory.createDecorator(multiply, "division");
        MyIntegerDecorator subtract = decoratorFactory.createDecorator(divide, "subtraction");

        assertEquals(4, add.getValue());
        assertEquals(8, multiply.getValue());
        assertEquals(4, divide.getValue());
        assertEquals(2, subtract.getValue());
        assertEquals(2, integer.getValue());

    }
}